from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='pyseas',
    version='3.0.0',
    description='Python Function collection for Oceanographic Instrumentation',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
        'Topic :: Scientific :: Data Processing :: Oceanographic Instrumentation :: QAQC :: OOI'
    ],
    keywords=['oceanography', 'instrumentation', 'seawater'],
    url='https://bitbucket.com/ooicgsn/pyseas/',
    author='Christopher Wingard',
    author_email='cwingard@coas.oregonstate.edu',
    license='MIT',
    packages=find_packages(),
    install_requires=[
      'geomag',
      'gsw',
      'nose',
      'numexpr',
      'numpy',
      'scipy'
    ],
    include_package_data=True,
    )
