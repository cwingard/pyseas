#!/usr/bin/env python
"""
@package pyseas.test.co2_functions
@file pyseas/test/co2_functions.py
@author Christopher Wingard
@brief Unit tests for co2_functions module
"""

import numpy as np
import unittest
from nose.plugins.attrib import attr

from pyseas.data import co2_functions as co2func


@attr('UNIT', group='func')
class Testpco2FunctionsUnit(unittest.TestCase):
    def setUp(self):
        # input test data for PCO2W
        self.mtype = np.array([5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4])
        self.ratio434 = np.array([11722, 2463, 2929, 2976, 2990, 2923, 2854, 2809, 2752, 2672, 2605, 2506, 2457, 2378])
        self.ratio620 = np.array([19228, 8813, 6144, 5807, 5567, 5659, 5919, 6186, 6560, 7119, 7577, 8388, 8749, 9389])
        self.traw = np.array([2243, 2238, 1848, 1953, 2060, 2131, 2182, 2197, 2201, 2203, 2204, 2208, 2224, 2254])

        # reagent constants (instrument and reagent bag specific)
        self.calt = 4.6539
        self.cala = 0.0422
        self.calb = 0.6761
        self.calc = -1.5798

        # expected outputs
        self.therm = np.array([7.3151, 7.4258, 16.2306, 13.8108, 11.3900, 9.8019, 8.6674,
                               8.3345, 8.2458, 8.2014, 8.1792, 8.0905, 7.7359, 7.0716])
        self.pco2 = np.array([np.nan, 609.8626, 394.3221, 351.6737, 321.4986, 324.067, 339.4317,
                              358.3335, 388.1735, 436.8431, 481.1713, 566.8172, 607.5355, 685.8555])

    def test_co2_pco2wat(self):
        """
        Test co2_pco2wat function.

        Values based on those described in DPS as available on Alfresco:

        OOI (2018). Data Product Specification for Partial Pressure of CO2 in
            Seawater. Document Control Number 1341-00490.
            https://alfresco.oceanobservatories.org/ (See: Company Home >> OOI
            >> Controlled >> 1000 System Level >>
            1341-00490_Data_Product_SPEC_PCO2WAT_OOI.pdf)

        Implemented by Christopher Wingard, April 2013
        """
        # compute the thermistor temperature in deg_C, derive blanks and then calculate pco2.
        tout = co2func.co2_thermistor(self.traw)
        np.testing.assert_allclose(tout, self.therm, rtol=1e-4, atol=1e-4)

        k434 = k620 = np.nan
        pco2 = np.ones(14) * np.nan
        for i in range(len(self.mtype)):
            if self.mtype[i] == 5:
                k434 = co2func.co2_blank(self.ratio434[i])
                k620 = co2func.co2_blank(self.ratio620[i])
            else:
                pco2[i] = co2func.co2_pco2wat(self.ratio434[i], self.ratio620[i], tout[i], self.calt,
                                              self.cala, self.calb, self.calc, k434, k620)

        np.testing.assert_allclose(pco2, self.pco2, rtol=1e-4, atol=1e-4)

    def test_co2_ppressure(self):
        """
        Test co2_ppressure function.

        Values based on those described in DPS as available on Alfresco:

        OOI (2012). Data Product Specification for Partial Pressure of CO2 in
            Air and Surface Seawater. Document Control Number 1341-00260.
            https://alfresco.oceanobservatories.org/ (See: Company Home >> OOI
            >> Controlled >> 1000 System Level >>
            1341-00260_Data_Product_SPEC_PCO2ATM_PCO2SSW_OOI.pdf)

        Implemented by Christopher Wingard, October 2014
        """
        test_data = np.array([
            [674, 1000, 665.19],
            [619, 1000, 610.91],
            [822, 1000, 811.25],
            [973, 1000, 960.28],
            [941, 1000, 928.69],
            [863, 1000, 851.71],
            [854, 1000, 842.83],
            [833, 1000, 822.11],
            [826, 1000, 815.20],
            [814, 1000, 803.36],
            [797, 1000, 786.58],
            [782, 1000, 771.77],
            [768, 1000, 757.96],
            [754, 1000, 744.14],
            [740, 1000, 730.32]
        ])
        ppres = co2func.co2_ppressure(test_data[:, 0], test_data[:, 1])
        np.testing.assert_allclose(ppres, test_data[:, 2], rtol=1e-2, atol=1e-2)

    def test_co2_co2flux(self):
        """
        Test co2_co2flux function.

        Values based on those described in DPS as available on Alfresco:

        OOI (2012). Data Product Specification for Flux of CO2 into the
            Atmosphere. Document Control Number 1341-00270.
            https://alfresco.oceanobservatories.org/ (See: Company Home >> OOI
            >> Controlled >> 1000 System Level >>
            1341-00270_Data_Product_SPEC_CO2FLUX_OOI.pdf)

        Implemented by Christopher Wingard, April 2013
        """
        test_data = np.array([
            [360, 390, 5, 0, 34, -2.063e-08],
            [360, 390, 5, 0, 35, -2.052e-08],
            [360, 390, 5, 10, 34, -1.942e-08],
            [360, 390, 5, 10, 35, -1.932e-08],
            [360, 390, 5, 20, 34, -1.869e-08],
            [360, 390, 5, 20, 35, -1.860e-08],
            [360, 390, 10, 0, 34, -8.250e-08],
            [360, 390, 10, 0, 35, -8.207e-08],
            [360, 390, 10, 10, 34, -7.767e-08],
            [360, 390, 10, 10, 35, -7.728e-08],
            [360, 390, 10, 20, 34, -7.475e-08],
            [360, 390, 10, 20, 35, -7.440e-08],
            [360, 390, 20, 0, 34, -3.300e-07],
            [360, 390, 20, 0, 35, -3.283e-07],
            [360, 390, 20, 10, 34, -3.107e-07],
            [360, 390, 20, 10, 35, -3.091e-07],
            [360, 390, 20, 20, 34, -2.990e-07],
            [360, 390, 20, 20, 35, -2.976e-07],
            [400, 390, 5, 0, 34, 6.875e-09],
            [400, 390, 5, 0, 35, 6.839e-09],
            [400, 390, 5, 10, 34, 6.472e-09],
            [400, 390, 5, 10, 35, 6.440e-09],
            [400, 390, 5, 20, 34, 6.229e-09],
            [400, 390, 5, 20, 35, 6.200e-09],
            [400, 390, 10, 0, 34, 2.750e-08],
            [400, 390, 10, 0, 35, 2.736e-08],
            [400, 390, 10, 10, 34, 2.589e-08],
            [400, 390, 10, 10, 35, 2.576e-08],
            [400, 390, 10, 20, 34, 2.492e-08],
            [400, 390, 10, 20, 35, 2.480e-08],
            [400, 390, 20, 0, 34, 1.100e-07],
            [400, 390, 20, 0, 35, 1.094e-07],
            [400, 390, 20, 10, 34, 1.036e-07],
            [400, 390, 20, 10, 35, 1.030e-07],
            [400, 390, 20, 20, 34, 9.966e-08],
            [400, 390, 20, 20, 35, 9.920e-08],
            [440, 390, 5, 0, 34, 3.438e-08],
            [440, 390, 5, 0, 35, 3.420e-08],
            [440, 390, 5, 10, 34, 3.236e-08],
            [440, 390, 5, 10, 35, 3.220e-08],
            [440, 390, 5, 20, 34, 3.114e-08],
            [440, 390, 5, 20, 35, 3.100e-08],
            [440, 390, 10, 0, 34, 1.375e-07],
            [440, 390, 10, 0, 35, 1.368e-07],
            [440, 390, 10, 10, 34, 1.294e-07],
            [440, 390, 10, 10, 35, 1.288e-07],
            [440, 390, 10, 20, 34, 1.246e-07],
            [440, 390, 10, 20, 35, 1.240e-07],
            [440, 390, 20, 0, 34, 5.500e-07],
            [440, 390, 20, 0, 35, 5.471e-07],
            [440, 390, 20, 10, 34, 5.178e-07],
            [440, 390, 20, 10, 35, 5.152e-07],
            [440, 390, 20, 20, 34, 4.983e-07],
            [440, 390, 20, 20, 35, 4.960e-07]
        ])

        # setup inputs and outputs
        pco2w = test_data[:, 0]
        pco2a = test_data[:, 1]
        u10 = test_data[:, 2]
        t = test_data[:, 3]
        s = test_data[:, 4]
        flux = test_data[:, 5]

        # compute the flux given the inputs
        out = co2func.co2_co2flux(pco2w, pco2a, u10, t, s)

        # and compare the results
        self.assertTrue(np.allclose(out, flux, rtol=1e-9, atol=1e-9))
